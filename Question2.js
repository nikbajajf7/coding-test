/**
 * Usage:
 * 
 * console.log(checker(stringToBePassed)) It will return no of required letters.
 */


function checker(string) {
  let lettersToInsert = 0
  const regexs = [/[a-z]/g, /[A-Z]/g, /[0-9]/g, /[!@#$%^&*()-+]/g]
  regexs.map((reg) => {
    const isChecked = string.match(reg)
    if (!(isChecked && isChecked.length)) {
      lettersToInsert += 1
    }
  })
  lettersToInsert = hasSixletters(string, lettersToInsert)
  return lettersToInsert
}

function hasSixletters(str, len) {
  const hasSix = 6 - str.length
  if (hasSix > 0) {
    if (hasSix > len) {
      return hasSix
    }
    return len
  }
  if (len) return len
  return 0
}
