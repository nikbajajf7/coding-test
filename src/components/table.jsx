import React from "react";
import {
  Button,
  Container,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Link,
} from "@material-ui/core";

const AuthorTable = ({ rows, fetched, selectRow, sortData }) => {
  if (!fetched) {
    return null;
  }

  return (
    <>
      {Boolean(rows.length) && (
        <div className="action-buttons">
          <Button
            variant="contained"
            color="primary"
            onClick={() => sortData("date")}
          >
            Newest
          </Button>
          <Button
            variant="contained"
            color="primary"
            className="top-button"
            onClick={() => sortData("upvotes")}
          >
            Top
          </Button>
        </div>
      )}
      <TableContainer component={Paper}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell component="th" scope="row">
                Title
              </TableCell>
              <TableCell align="right">Upvotes</TableCell>
              <TableCell align="right">Date</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.length ? (
              rows.map((row) => (
                <TableRow key={row.articleId}>
                  <TableCell component="th" scope="row">
                    <Link href="#" onClick={() => selectRow(row.articleId)}>
                      {row.title}
                    </Link>
                  </TableCell>
                  <TableCell align="right">{row.upvotes}</TableCell>
                  <TableCell align="right">{row.date}</TableCell>
                </TableRow>
              ))
            ) : (
              <TableRow>
                <TableCell component="th" scope="row">
                  'No data'
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default AuthorTable;
