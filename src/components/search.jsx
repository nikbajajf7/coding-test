import React, { useCallback, useState } from "react";
import { Button, Grid, TextField } from "@material-ui/core";

const Search = ({ handleFetch, searchParam }) => {
  const [inputValue, setInputValue] = useState(searchParam || "");

  const handleInputValue = useCallback((e) => {
    setInputValue(e.target.value);
  }, [setInputValue]);

  return (
    <Grid item container justify="center">
      <div className="search-box">
        <TextField
          value={inputValue}
          onChange={handleInputValue}
          variant="outlined"
          size="small"
          label="Author Name"
          placeholder="Enter a name"
        />
        <Button
          variant="contained"
          color="primary"
          onClick={() => handleFetch(inputValue)}
          disabled={!inputValue}
          className="button-fetch"
        >
          Fetch
        </Button>
      </div>
    </Grid>
  );
};

export default Search;
