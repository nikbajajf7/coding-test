import { Box, Button, Container } from "@material-ui/core";
import React from "react";

const Details = ({ getBack, content }) => (
  <Container maxWidth="sm">
    <Box className="box">
      <Button variant="contained" color="primary" onClick={getBack}>
        back
      </Button>
      <div>
        <h2 className="details-title">Details</h2>
        <p className="content-para">{content}</p>
      </div>
    </Box>
  </Container>
);

export default Details;
