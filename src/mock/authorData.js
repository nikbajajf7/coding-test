const rows = [
  {
    articleId: 1,
    title: 'Harry Potter and the Sorcerer’s Stone Review',
    upvotes: 56,
    date: '2016-12-23',
    authorName: 'JK Rowling'
  },
  {
    articleId: 2,
    title: 'Harry Potter and the Half Blood Prince Review',
    upvotes: 23,
    date: '2016-12-02',
    authorName: 'JK Rowling'
  },
  {
    articleId: 3,
    title: 'Harry Potter and the Goblet of Fire Review',
    upvotes: 3,
    date: '2017-11-02',
    authorName: 'JK Rowling'
  },
  {
    articleId: 4,
    title: ' The Fellowship of the Ring',
    upvotes: 86,
    date: '2012-12-23',
    authorName: 'JRR Tolkien'
  },
  {
    articleId: 5,
    title: 'The Two Towers',
    upvotes: 80,
    date: '2013-12-02',
    authorName: 'JRR Tolkien'
  },
  {
    articleId: 6,
    title: 'The Return of the King',
    upvotes: 60,
    date: '2014-11-02',
    authorName: 'JRR Tolkien'
  }
]

const contentData = [
  {
    articleId: 1,
    content: 'Harry Potter, an eleven-year-old orphan, discovers that he is a wizard and is invited to study at Hogwarts. Even as he escapes a dreary life and enters a world of magic, he finds trouble awaiting him.'
  },
  {
    articleId: 2,
    content: `Dumbledore and Harry Potter learn more about Voldemort's past and his rise to power. Meanwhile, Harry stumbles upon an old potions textbook belonging to a person calling himself the Half-Blood Prince.
    `
  },
  {
    articleId: 3,
    content: `When Harry is chosen as a fourth participant of the inter-school Triwizard Tournament, he is unwittingly pulled into a dark conspiracy that endangers his life.`
  },
  {
    articleId: 4,
    content: `A young hobbit, Frodo, who has found the One Ring that belongs to the Dark Lord Sauron, begins his journey with eight companions to Mount Doom, the only place where it can be destroyed.`
  },
  {
    articleId: 5,
    content: `Frodo and Sam arrive in Mordor with the help of Gollum. A number of new allies join their former companions to defend Isengard as Saruman launches an assault from his domain.`
  },
  {
    articleId: 6,
    content: `The former Fellowship members prepare for the final battle. While Frodo and Sam approach Mount Doom to destroy the One Ring, they follow Gollum, unaware of the path he is leading them to.`
  }
]

export const getContent = (id) => (contentData.find((ele) => ele.articleId === id))

export const getData = (name) => (rows.filter((ele) => ele.authorName === name))