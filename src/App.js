import React, { useState, useCallback } from 'react';
import { Container } from '@material-ui/core';
import Datatable from './components/table'
import SearchBar from './components/search'
import SelectedRow from './components/authorDetail'
import { getData, getContent } from './mock/authorData'
import './App.css';

const App = () => {
  const [data, setData] = useState([])
  const [fetchedFor, setFetched] = useState(null)
  const [selectedRow, setSelectedRow] = useState(null)
  const [selectedRowData, setSelectedRowData] = useState(null)

  const handleFetch = useCallback((name) => {
    const AuthorData = getData(name)
    setData(AuthorData)
    setFetched(name)
  }, [setData, setFetched])

  const selectRow = useCallback((id) => {
    const res = getContent(id)
    setSelectedRowData(res.content)
    setSelectedRow(id)
  }, [setSelectedRowData, setSelectedRow])

  const backToList = useCallback(() => {
    setSelectedRow(null)
    setSelectedRowData(null)
  }, [])

  const sortTableData = useCallback((sortBy) => {
    switch (sortBy) {
      case 'date': {
        const sortedData = data.slice().sort((a, b) => new Date(b.date) - new Date(a.date))
        setData(sortedData)
        break
      }
      case 'upvotes': {
        const sortedData = data.slice().sort((a, b) => b.upvotes - a.upvotes)
        setData(sortedData)
        break
      }
      default: {
        return null
      }
    }
  })

  if (selectedRow) {
    return <SelectedRow content={selectedRowData} getBack={backToList} />
  }

  return (
    <Container maxWidth="md">
      <div className='box'>
        <SearchBar handleFetch={handleFetch} searchParam={fetchedFor} />
        <Datatable rows={data} fetched={fetchedFor} selectRow={selectRow} sortData={sortTableData} />
      </div>
    </Container>
  );
}

export default App;
